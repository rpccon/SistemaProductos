/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemaproductos;

import java.util.LinkedList;
import java.util.Scanner;

/**
 *
 * @author Johanny
 */
public class Index {
    
    
    Scanner user_input ;
    LinkedList<Producto> listaProd;
    LinkedList<String> carCompras;
    
    public Index(){
        user_input = new Scanner( System.in ); // tomar datos del cliente
        this.listaProd = new LinkedList<Producto>();
        this.carCompras = new LinkedList<String>();
    }
    
    public void setData(){
        
        Producto p1 = new Producto();
        Producto p2 = new Producto();
        Producto p3 = new Producto();
        Producto p4 = new Producto();
        Producto p5 = new Producto();
        
        p1.setID("1");
        p1.setNombre("Tarjeta gráfica NGForce 1080");
        p1.setCantidad(50);
        p1.setPrecio(23800);

        p2.setID("2");        
        p2.setNombre("Disco SSD 512 GB");
        p2.setCantidad(35);
        p2.setPrecio(156000);
        
        p3.setID("3");        
        p3.setNombre("Disco HDD 1 TB");
        p3.setCantidad(60);
        p3.setPrecio(107000);

        p4.setID("4");
        p4.setNombre("Encapsulador 2.0");
        p4.setCantidad(15);
        p4.setPrecio(10000);

        p5.setID("5");
        p5.setNombre("Memoria RAM 8 GB DDR3");
        p5.setCantidad(67);
        p5.setPrecio(16754);   
        
        listaProd.add(p1);
        listaProd.add(p2);
        listaProd.add(p3);
        listaProd.add(p4);
        listaProd.add(p5);

    }
    
    public void imprimirProd(){
        
        String buffer="Lista productos: \n";
         
        for(int i=0;i<listaProd.size();i++){
            buffer+="   "+String.valueOf(i+1)+"."+listaProd.get(i).getNombre();
            buffer+="\n        ----> Cantidad Inv: "+String.valueOf(listaProd.get(i).getCantidad());
            buffer+="\n        ----> Precio: "+String.valueOf(listaProd.get(i).getPrecio());
            buffer+="\n";
        }
        
        System.out.println(buffer);
    }
    
    public void Regresar(){
        System.out.println("Presione cualquier tecla y enter para regresar a menu.....");
        String num = user_input.next();
        Menu();
    }
    
    public void reducirInvProd(String id){// reduce la cantidad de productos del inventario
        for(int e = 0;e<listaProd.size();e++){
            if(listaProd.get(e).getID().equals(id)){
                listaProd.get(e).setCantidad(listaProd.get(e).getCantidad()-1);
                break;
            }
        }        
    }
    
    public int obtCantProducto(String id){
        int cant = 0;
        for(int ej = 0;ej<listaProd.size();ej++){
            if(listaProd.get(ej).getID().equals(id)){
                cant = listaProd.get(ej).getCantidad();
                break;
            }
        } 
        return cant;
    }
    public int obtPrecioProducto(String id){
        int precio = 0;
        for(int ej = 0;ej<listaProd.size();ej++){
            if(listaProd.get(ej).getID().equals(id)){
                precio = listaProd.get(ej).getPrecio();
                break;
            }
        } 
        return precio;
    }
    
    public int validarCompra(String id){ // return 1 -> no existe // 2 -> compra exitosa // 3 -> ya hay un artículo en el carrito
        
        int cantidad = obtCantProducto(id);

        if(cantidad!=0){ // producto existente en la lista
            int find = 0;
         
            for(int i=0;i<carCompras.size();i++){
                if(carCompras.get(i).equals(id)){

                    find+=1;
                }
            }
            if(find<2){
                if(cantidad >= 1 ){ // se realiza la compra
                    this.carCompras.addLast(id);
                    reducirInvProd(id);
                    return 1; // compra satisfactoria
                }
                else{
                    return 2; // No se puede comprar, inventario insuficiente
                }
            }
            else{
                return 3; // ya existe un producto en el carrito igual
            }            
        }
        else{// no existe en la lista
            return 0;
        }        
        
    }
    public int obtenerTotalCarrito(){
        
        int total=0;
        
        for(int i=0;i<carCompras.size();i++){
            total+= obtPrecioProducto(carCompras.get(i));
        }
        
        return total;
        
        
    }
    
    public void realizarCompra(){
        System.out.println("Para realizar una compra ingrese el número(id) de producto y presione enter\n(Para ver productos vaya a la opción 1 del menú principal)\n"
                + "X.Para dejar de agregar al carrito\n"
                + "Y.Limpiar carrito\n"
                + "Z.Realizar nueva compra(Mostrar total a pagar)");
        while(true){
            System.out.println("\nIndique el número del producto...");
            String num = user_input.next();
            if(num.equals("X")){
                break;
            }
            else if(num.equals("Y")){
                this.carCompras.clear();
                pMsj("---->Carrito de compras limpiado satisfactoriamente");
                
            }
            else if(num.equals("Z")){
                
                int total = obtenerTotalCarrito();
                
                if(total == 0){
                    pMsj("Error...No ha agregado nada al carrito..");
                }
                else{
                  pMsj("Compra realizada satisfactoriamente\n"
                        + "------------>Usted debe pagar: "
                        + String.valueOf(total));   
                  break;
                }
            }
            else{
               int result = validarCompra(num);

               if(result==0){
                   System.out.println("---->No existe ningún producto con el número indicado.");
               }
               else if(result==1){
                   pMsj("---->Agregado al carrito satisfactoriamente");
               }
               else if(result==2){
                   pMsj("---->Inventario insuficiente");
               }
               else{
                   pMsj("---->Error, ya existe una compra de ese producto en la lista");
               }               
            }

        }
    
    }
    
    public void pMsj(String mensaje){// función para imprimir mensaje en consola más eficientemente
        System.out.println(mensaje);
    }

    public void Menu(){
        
        String data ="***********Bienvenido al sistema de compras***********\n"+
                "1. Para ver listado de productos\n"+
                "2. Realizar compra";     
        System.out.println(data);        
        String num = user_input.next();
        
        if(num.equals("1")){
            imprimirProd();
        }
        else if(num.equals("2")){
            realizarCompra();
        }
        Regresar();
    }
    
    
}
