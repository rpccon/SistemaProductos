/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemaproductos;

/**
 *
 * @author Johanny
 */
public class Producto {
    

    String nombre;
    int cantidad;
    int precio;
    String id;
    
    public Producto(){
      
        this.nombre="";
        this.cantidad=0;
        this.precio=0;
        this.id="";
    }

    public String getNombre() {
        return nombre;
    }
    public String getID() {
        return id;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public void setID(String id) {
        this.id = id;
    }
    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }
    
    
    
    
}
